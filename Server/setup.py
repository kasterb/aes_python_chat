import re
from cx_Freeze import setup, Executable

setup(
    name = "Cryptcat V1.0 - Server",
    version = "1.0",
    description = "Cryptcat V1.0 - Server -- Guillaume Monarque 2014 ",
    executables = [Executable("Cryptcat_server.py")],
)
