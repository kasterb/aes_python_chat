#Guillaume Monarque 2014

import socket
import urllib.request
import os

local_ip = socket.gethostbyname(socket.gethostname())
ext_ip = str(urllib.request.urlopen('http://myip.dnsdynamic.org/').read())
ext_ip = ext_ip[2:len(ext_ip)-1]

print("Hello! This is the server configuration assistant")
print("This will only work over the internet. In local, don't change anything\n")
print("This is the TCP_IP you have to put in config.ini: ",local_ip)
print("You have to forward the port(TCP) set in config.ini, with this IP address as destination: ",local_ip)
print("The client must connect to this IP address (your external one): ",ext_ip,"\n")
print("Here you go!")

os.system("pause") 
