from Crypto.Cipher import AES
import binascii
import os
import random
import string
import socket
import sys
from threading import Thread
import time



exec(open("config.ini").read())


class Receive(Thread):

    def __init__(self):
        Thread.__init__(self)
        

    def run(self):
        while 1:
            try:
               msgr = socket_client.recv(BUFFER_SIZE)
            except:
               print("[*]The client is offline")
               sys.exit()
            msgr = msgr.decode('UTF-8')
            ciphertext = msgr
            lenkeynumber = ciphertext[0]
            lenkeynumber = int(lenkeynumber)
            lenkeynumber = lenkeynumber +1
            keynumber = ciphertext[1:lenkeynumber]
            ciphertext = ciphertext[lenkeynumber:len(ciphertext)]

            file = open('keys.txt', 'r')
            var = 0
            var = int(var)
            keynumber = int(keynumber)
            while (var != keynumber):
                key = file.readline()
                var = var+1
            key = key[0:32]

            iv = ciphertext[0:32]
            iv = binascii.unhexlify(iv)

            lenciphertext = len(ciphertext)
            ciphertext = ciphertext[32:lenciphertext]


            ciphertext = binascii.unhexlify(ciphertext)
            decobj = AES.new(key, AES.MODE_CBC,iv)
            plaintext = decobj.decrypt(ciphertext)
            plaintext = str(plaintext)
            plaintext = plaintext.replace("~"," ")
            msgr = plaintext
            lenmsgr = len(msgr)
            msgr = msgr[2:lenmsgr-1]

            print(msgr+"\a")
            sys.stdout.flush()
            msgr=""
            

            
class Send(Thread):

    def __init__(self):
        Thread.__init__(self)
        

    def run(self):
        while 1:
            plaintext = input()
            if (plaintext == "/help"):
               print("[*] Commands available: /help /quit /status /cryptinfos /send /about ")
            elif (plaintext == "/quit"):
               print("[*] You are disconnected")
            elif (plaintext == "/status"):
                if Receive_thread.is_alive():
                    print("[*] The receive thread is alive")
                else:
                    print("[*] The receive thread is dead")
                if Send_thread.is_alive():
                    print("[*] The send thread is alive")
                else:
                    print("[*] The send thread is dead")  
            elif (plaintext == ""):
               print("[*] You can't send an empty message")
            elif (plaintext == "/sh0ck"):
               print("[*] h4x0r 1337 MLG READY w00t pWwWn33d 3/6 FLACHKOPP")
            elif (plaintext == "/about"):
                print("[*] CryptCat V1.0 by Guillaume Monarque - 2014")
            elif (plaintext == "/cryptinfos"):
                try:
                   print("[*] The last key used to crypt is",key)
                   print("[*] It is the key number",keynumber)
                except:
                    print("[*] Please send at least one message first")
            elif (plaintext == "/send"):
                try:
                    file = int(input("[*] Please input the number corresponding to the file to send\n[*] "))
                    print(file)
                except:
                    print("[*] Please input a number")
            elif (plaintext[0] == "/"):
                print("[*] Command unknown")


            else:
               plaintext = "<"+PSEUDO+"> "+plaintext
               plaintext = plaintext.replace(" ","~")
               lenmessage = len(plaintext)
               lenmassage = int(lenmessage)

               if (lenmessage < 16):
                  padding = 16-lenmessage
                  plaintext = plaintext+("~"*padding)

               elif (lenmessage < 32):
                  padding = 32-lenmessage
                  plaintext = plaintext+("~"*padding)

               elif (lenmessage < 64):
                  padding = 64-lenmessage
                  plaintext = plaintext+("~"*padding)

               elif (lenmessage < 128):
                  padding = 128-lenmessage
                  plaintext = plaintext+("~"*padding)

               elif (lenmessage < 256):
                  padding = 256-lenmessage
                  plaintext = plaintext+("~"*padding)

               elif (lenmessage < 512):
                  padding = 512-lenmessage
                  plaintext = plaintext+("~"*padding)

               elif (lenmessage > 512):
                  padding = 32-lenmessage
                  plaintext = "Message is too long (>512)"

               iv = os.urandom(16)
               file = open('keys.txt', 'r')
               var = 0
               keynumber = random.randint(2, 1499)


               while (var != keynumber):
                  key = file.readline()
                  var = var+1
               key = key[0:32]
               keynumber = str(keynumber)
               lenkeynumber = len(keynumber)
               lenkeynumber = str(lenkeynumber)

               keydata = lenkeynumber+keynumber
               keydata = keydata.encode('UTF-8')

               encobj = AES.new(key, AES.MODE_CBC,iv)
               try:
                  ciphertext = encobj.encrypt(plaintext)
               except:
                  print("[*] An error occured while crypting the message")
                  ciphertext = "error"
               if (ciphertext != "error"):
                   ciphertext = binascii.hexlify(iv+ciphertext)
                   ciphertext = keydata+ciphertext
                   msgs = ciphertext
                   try:
                       socket_client.send(msgs)
                   except:
                       print("[*] An error occured while sending the message")
                  
               sys.stdout.flush()
               msgs=""

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.bind((TCP_IP, TCP_PORT))
socket.listen(7)

print("[*] The server is up. Waiting for the client to connect...")
print("[*] Listenning on",TCP_IP,":",TCP_PORT)
socket_client, infos = socket.accept()
print("[*] The client",infos,"is connected!")
print("[*] You can now write messages and send them with 'Enter'\n\n")



Receive_thread = Receive()
Send_thread = Send()

Receive_thread.start()
Send_thread.start()




    

