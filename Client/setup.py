import re
from cx_Freeze import setup, Executable

setup(
    name = "Cryptcat V1.0 - Client",
    version = "1.0",
    description = "Cryptcat V1.0 - Client -- Guillaume Monarque 2014 ",
    executables = [Executable("Cryptcat_client.py")],
)
