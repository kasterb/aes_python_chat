#Made by Guillaume Monarque - 2014

import random
import string

def key(length):
   return ''.join(random.choice(string.hexdigits) for i in range(length))

keycount = input("How many keys to be generated?")
filename = input("How to name the .txt file?")
keycount = int(keycount)
filename = str(filename)
var = 0
file=open(filename+".txt", "w")
while (var < keycount):
    var=var+1
    file.write(key(32)+"\n")
file.close()
print("Keys generated")
