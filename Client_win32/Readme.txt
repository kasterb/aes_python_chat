v1.0 Beta 2014
Guillaume Monarque

############

This is a "secure" chat software. It's gonna be secure when the data is transiting from one computer to another: everything that is received and sent is crypted in AES 256 with one of the random 1500 keys that are in keys.txt. This means that man-in-the-middle attacks are useless, sniffing the network won't reveal the messages. Please be sure to keep everything in the same folder! You and your partner MUST have the same keys.txt! If you rename keys.txt or delete it, you won't be able to see or send messages, since you won't be able to use the encryption part of this software. 

############

